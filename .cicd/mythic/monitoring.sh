#!/bin/bash

helm repo add bitnami https://charts.bitnami.com/bitnami && helm repo list
kubectl create namespace monitoring
helm install prometheus bitnami/kube-prometheus -n monitoring --set prometheus.service.type=NodePort --set prometheus.service.nodePort=30010
kubectl get deployments,pods,services --selector app.kubernetes.io/instance=prometheus -n monitoring
#touch datasource-prometheus.yaml
#echo "
#datasources:
#  - name: Prometheus
#    type: prometheus
#    url: http://prometheus-kube-prometheus-prometheus:9090
#    editable: true
#    isDefault: false
#" | tee datasource‑prometheus.yaml

kubectl create secret generic datasource-secret -n monitoring --from-file=datasource-prometheus.yaml
wget -c https://grafana.com/api/dashboards/18283/revisions/1/download -o 18283_rev1.json
kubectl create configmap dashboard -n monitoring --from-file=18283_rev1.json

#touch grafana.values.yaml
#echo "
#service:
#  type: NodePort
#  nodePorts:
#    grafana: 30011
#
#persistence:
#  enabled: false
#
#dashboardsProvider:
#  enabled: true
#
#datasources:
#  secretName: datasource-secret
#
#dashboardsConfigMaps:
#  - configMapName: dashboard-1
#    filename: 18283_rev1.json
#" | tee grafana.values.yaml

helm install grafana bitnami/grafana -n monitoring --values grafana.values.yaml
echo "User: admin"
echo "Password: $(kubectl get secret grafana-admin --namespace monitoring -o jsonpath="{.data.GF_SECURITY_ADMIN_PASSWORD}" | base64 -d)"
kubectl get deployments,pods,services -n monitoring --selector app.kubernetes.io/instance=grafana

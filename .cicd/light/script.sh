#!/bin/bash

### Install sys reauirements ###
sudo apt update
sudo apt-get install -y python3-pip
pip3 install virtualenv
sudo apt-get install -y libpq-dev gcc
sudo apt install -y python3.8-venv

### Clone app ###
git clone https://gitfront.io/r/deusops/Fsjok1dx89xG/flask-project-01.git
cd flask-project-01

### Install with virtualenv ###
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt

### Initalize and seed the database ###
flask db upgrade
python seed.py

### Running tests ###
python -m unittest discover

### Running app on a production server ###
pip install -r requirements-server.txt
gunicorn app:app -b 0.0.0.0:8000
